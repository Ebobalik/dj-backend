from flask import Flask,request,jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from yandex_music import ClientAsync,Client
from youtube_dl import YoutubeDL
from bs4 import BeautifulSoup
import requests


class MyFlaskApp(Flask):
  def run(self, host=None, port=None, debug=None, load_dotenv=True, **options):
    if 1:
      with self.app_context():
        print("STARTED LOADING FROM DB")
        for track in Track.query.all():
            tracks_queue.append(track)
            print(track)
    super(MyFlaskApp, self).run(host=host, port=port, debug=debug, load_dotenv=load_dotenv, **options)


class YandexTool:
    def __init__(self,token):
        self.client =  Client(token).init()
        self.token = token

    def GetURLById(self,album_id,song_id):
        url = self.client.tracks_download_info(f'{song_id}:{album_id}',get_direct_links=True)
        return url[0]['direct_link']
    
    def GetName(self,url):
        album_id = int(url.split('/')[4])
        track_id = int(url.split('/')[6].split("?")[0])
        res = yandex.client.tracks(f'{track_id}:{album_id}')
        response={}
        response['title']=res[0]['title']
        response['artist']=res[0]['artists'][0]['name']
        print(response)
        return response

app = MyFlaskApp(__name__)
app.config["SECRET_KEY"]="verysecretkey"
app.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql://dj-admin:verysecretkey@localhost:5432/dj-db"
CORS(app)
db = SQLAlchemy(app)
yandex = YandexTool('AQAAAABf5kClAAG8XjSxQFMT90ANtU7l1hYt6nk')
tracks_queue = []
youtube = YoutubeDL()


def get_youtube_info(url):
   response = requests.get(url)
   #response.html.render(sleep=1)
   soup = BeautifulSoup(response.text,'html.parser')
   result = {}
   result["title"] = soup.find("meta", itemprop="name")['content']
   print(result)
   return result

class Track(db.Model):
    __tablename__ = 'tracks'
    url = db.Column(db.Text,primary_key=True)
    type = db.Column(db.Text)
    name= db.Column(db.Text)


@app.route("/delete_track/",methods=['GET'])
def delete_track():
    url = request.args.get("url")
    tracks = Track.query.all()
    for track in tracks:
       if track.url == url:
          db.session.delete(track)
    db.session.commit()
    return jsonify(message="all tracks with this url was deleted")
       
@app.route("/send_track",methods=['POST'])
def send_track():
    url = request.args.get("url")
    try:
        if url.find("youtube")!=-1 or url.find("youtu")!=-1:
            data = get_youtube_info(url)
            track = Track(url=url,type='youtube',name=data['title'])
        elif url.find("music.yandex")!=-1:
            name_dict = yandex.GetName(url)
            track = Track(url=url,type='yandex',name=name_dict['title']+" - "+name_dict['artist'])
        else:
            return jsonify(message="failed"),405
        db.session.add(track)
        db.session.commit()
        return jsonify(message='success')
    except:
       return jsonify(message='Track already added'),400
    
@app.route('/fetch_tracks',methods=['GET'])
def get_tracks():
    tracks = Track.query.all()
    response = []
    for track in tracks:
        temp = {}
        temp['url'] = track.url
        temp['type'] = track.type
        temp['name'] = track.name
        #response.append((temp['url'],temp['type'],temp['name']))
        response.append(temp)
    #print(response)
    return response

@app.route('/fetch_track_url')
def fetch_track_url():
   url = request.args.get("url")
   track = Track.query.filter(url=url).first()
   if not track:
      return jsonify(message='Failed')
   if track.type == "youtube":
      return jsonify(url=url)
   return jsonify(url='yandex placehoder')

@app.route("/get_next/<int:num>/")
def get_next_track(num):
    if tracks_queue[num].type=="youtube":
       return tracks_queue[num].url
    else:
        url = tracks_queue[num].url
        album_id = int(url.split('/')[4])
        track_id = int(url.split('/')[6].split("?")[0])
        return yandex.GetURLById(album_id,track_id)

if __name__=="__main__":
   app.run()